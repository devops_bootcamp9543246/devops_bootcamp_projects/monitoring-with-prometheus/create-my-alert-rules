# 16 - Monitoring with Prometheus - Create own Alert Rules

**Demo Project:**
Configure Alerting for our Application

**Technologies used:**
Prometheus, Kubernetes, Linux

**Project Description:**
- Configure our Monitoring Stack to notify us whenever CPU usage > 50% or Pod cannot start
    - Configure Alert Rules in Prometheus Server 
    - Configure Alertmanager with Email Receiver




16 - Monitoring with Prometheus
# This project was developed as part of the DevOps Bootcamp at Techword With NANA https://www.techworld-with-nana.com/devops-bootcamp





